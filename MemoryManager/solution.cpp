#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include "common.h"
#include <iostream>
#include <exception>
#include <math.h>
using namespace std;
#endif /* __PROGTEST__ */

#define SAFE_DELETE(x) { if (x) { delete (x); (x) = NULL; } }

class OutOfMemory : public exception {
    virtual const char* what() const throw() {
        return "out_of_memory";
    }
} g_OutOfMemory;

class CIntel;

struct ThreadArg {
    void *memStart;
    void* processArg;
    void (*entryPoint)(CCPU*, void*);
    bool copyMem;
    CIntel *parent_cpu;
};

class MemoryManager {
    uint32_t *memory;
    uint8_t *freePages;
    uint32_t neededPages;
    uint32_t numPages;
    pthread_mutex_t mtx;
    
public:
    uint32_t proc_count;
    pthread_cond_t proc_cond;
    pthread_mutex_t proc_mtx;
    
public:
    MemoryManager(void *m, uint32_t pages) {
        memory = (uint32_t*) m;
        numPages = pages;
        freePages = NULL;
        pthread_mutex_init(&mtx, NULL);
        
        proc_count = 0;
        pthread_cond_init(&proc_cond, NULL);
        pthread_mutex_init(&proc_mtx, NULL);
        
        freePages = (uint8_t*) memory;
        for (uint32_t i = 0; i < numPages; i++) {
            freePages[i] = 1;
        }
        neededPages = ceil( (double) numPages / CCPU::PAGE_SIZE );
        for (uint32_t i = 0; i < neededPages; i++) {
            try {
                getFreePage(false);
            } catch (exception& e) {
                
            }
        }
    }
    
    ~MemoryManager() {
        for (uint32_t i = 0; i < numPages; i++) {
            freePages[i] = 0;
        }
        pthread_mutex_destroy(&proc_mtx);
        pthread_cond_destroy(&proc_cond);
        pthread_mutex_destroy(&mtx);
    }
    
    uint32_t allocateFirstLevelTable() {
        //Alloc 1 page for level-1 table
        uint32_t level1_page = 0;
        try {
            level1_page = getFreePage(false);
        } catch (exception& e) {
            throw;
        }
        uint32_t level1_offset = level1_page << CCPU::OFFSET_BITS;
        uint8_t *m = (uint8_t *) memory;
        uint32_t *level1_mem_start = (uint32_t *)(m + level1_offset);
        //Alloc 1 page for level-2 table
        uint32_t level2_page = 0;
        try {
            level2_page = getFreePage(false);
        } catch (exception& e) {
            throw;
        }
        const uint32_t reqMask = CCPU::BIT_PRESENT | CCPU::BIT_USER | CCPU::BIT_WRITE;
        *level1_mem_start = reqMask;
        *level1_mem_start |= level2_page << CCPU::OFFSET_BITS; //save in level-1 table's 1st entry reference to 2-level table
        
        return level1_page << CCPU::OFFSET_BITS;
    }
    
    bool deallocateFirstLevelTable(uint32_t pageTableRoot) {
        const uint32_t reqMask = CCPU::BIT_PRESENT | CCPU::BIT_USER | CCPU::BIT_WRITE;
        uint32_t level1_offset = pageTableRoot & CCPU::ADDR_MASK;
        if (level1_offset > numPages) return false;
        uint8_t *m = (uint8_t *) memory;
        uint32_t* level1_entry_mem = (uint32_t *)(m + level1_offset);
        if ((*level1_entry_mem & reqMask) != reqMask) {
        }
        uint32_t level2_offset = *level1_entry_mem & CCPU::ADDR_MASK;
        uint32_t* level2_entry_mem = (uint32_t *)(m + level2_offset);
        *level2_entry_mem = 0;
        uint32_t page = level2_offset >> CCPU::OFFSET_BITS;
        pthread_mutex_lock(&mtx);
        freePages[page] = 1;
        pthread_mutex_unlock(&mtx);
        
        *level1_entry_mem = 0;
        page = level1_offset >> CCPU::OFFSET_BITS;
        pthread_mutex_lock(&mtx);
        freePages[page] = 1;
        pthread_mutex_unlock(&mtx);
        
        return true;
    }
    
    uint32_t getFreePage(bool init) {
        bool found = false;
        uint32_t page = -1;
        pthread_mutex_lock(&mtx);
        for (uint32_t i = 0; i < numPages && !found; i++) {
            if (freePages[i] == 1) {
                found = true;
                page = i;
                freePages[page] = 0;
            }
        }
        pthread_mutex_unlock(&mtx);
        if (!found) {
            throw g_OutOfMemory;
        }
        uint32_t offset = page << (CCPU::OFFSET_BITS - 2);
        uint32_t *page_start_mem = memory + offset;
        const uint32_t reqMask = CCPU::BIT_PRESENT | CCPU::BIT_USER | CCPU::BIT_WRITE;
        if (init) {
            for (uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i++) {
                *(page_start_mem + i) = reqMask;
            }
        }
        return page;
    }
    
    void deallocatePage(uint32_t page) {
        uint32_t offset = page << (CCPU::OFFSET_BITS - 2);
        uint32_t *page_start_mem = memory + offset;
        for (uint32_t i = 0; i < CCPU::PAGE_DIR_ENTRIES; i++) {
            *(page_start_mem + i) = 0;
        }
        pthread_mutex_lock(&mtx);
        freePages[page] = 1;
        pthread_mutex_unlock(&mtx);
    }
    
    void checkClearDump() {
        for (uint32_t iPage = 0; iPage < numPages; iPage++) {
            uint32_t *m = memory + iPage * CCPU::PAGE_DIR_ENTRIES;
            for (uint32_t entry = 0; entry < CCPU::PAGE_DIR_ENTRIES; entry++) {
                if (m[entry] != 0) {
                    cout << m[entry] << " page=" << iPage << " entry=" << entry << endl;
                }
            }
        }
    }
    
};

MemoryManager *g_Manager = NULL;

void thread_func(void *arg);

class CIntel : public CCPU
{
    uint32_t m_MemLimit;
    uint32_t m_LastLvl1Entry;
    uint32_t m_LastLvl2Entry;
    uint32_t m_ParentPageTableRoot;
public:
    static pthread_attr_t thr_attr;
    static const uint32_t reqMask = CCPU::BIT_PRESENT | CCPU::BIT_USER | CCPU::BIT_WRITE;
    static const uint32_t readOnly = CCPU::BIT_PRESENT | CCPU::BIT_USER;
    
public:
    CIntel(const CIntel& cpu)
        : CCPU(cpu.m_MemStart, cpu.m_PageTableRoot) {
        m_MemLimit = cpu.m_MemLimit;
        m_LastLvl1Entry = cpu.m_LastLvl1Entry;
        m_LastLvl2Entry = cpu.m_LastLvl2Entry;
    }
    
    CIntel(uint8_t* memStart, uint32_t pageTableRoot)
    : CCPU(memStart, pageTableRoot) {
        m_MemLimit = 0;
        m_LastLvl1Entry = 0;
        m_LastLvl2Entry = 0;
        m_ParentPageTableRoot = 0;
    }
    
    ~CIntel() {
        SetMemLimit(0);
        g_Manager->deallocateFirstLevelTable(m_PageTableRoot);
    }
    
    void setLastLvl1Entry(uint32_t lvl1_entry) {
        m_LastLvl1Entry = lvl1_entry;
    }
    
    uint32_t getLastLvl1Entry() const {
        return m_LastLvl1Entry;
    }
    
    void setLastLvl2Entry(uint32_t lvl2_entry) {
        m_LastLvl2Entry = lvl2_entry;
    }
    
    uint32_t getLastLvl2Entry() const {
        return m_LastLvl2Entry;
    }
    
    uint32_t getPageTableRoot() const {
        return m_PageTableRoot;
    }
    
    uint32_t getParentPageTableRoot() const {
        return m_ParentPageTableRoot;
    }
    
    void setParentPageTableRoot(uint32_t pTableRoot) {
        m_ParentPageTableRoot = pTableRoot;
    }
    
    virtual uint32_t GetMemLimit(void) const {
        return m_MemLimit;
    }
    
    virtual bool SetMemLimit (uint32_t pages) {
        bool status = true;
        const uint32_t reqMask = CCPU::BIT_PRESENT | CCPU::BIT_USER | CCPU::BIT_WRITE;
        uint32_t extra = 0;
        if (pages >= m_MemLimit) {
            //Alloc extra pages of memory
            extra = pages - m_MemLimit;
            uint32_t allocated = 0;
            while (allocated < extra) {
                uint32_t level1_offset = m_PageTableRoot & CCPU::ADDR_MASK;
                uint32_t* level1_entry_mem = (uint32_t *)(m_MemStart + level1_offset) + m_LastLvl1Entry;
                if ((*level1_entry_mem & reqMask) != reqMask) {
                }
                
                uint32_t level2_offset = *level1_entry_mem & CCPU::ADDR_MASK;
                uint32_t* level2_entry_mem = (uint32_t *)(m_MemStart + level2_offset) + m_LastLvl2Entry;
                if ((*level2_entry_mem & reqMask) != reqMask) {
                }
                uint32_t data_page = 0;
                try {
                    data_page = g_Manager->getFreePage(true);
                } catch (exception& e) {
                    return false;
                }
                *level2_entry_mem = reqMask;
                *level2_entry_mem |= data_page << CCPU::OFFSET_BITS;    //store in 2-level table entry reference to data page
                m_LastLvl2Entry++;
                if (m_LastLvl2Entry > 1023) {
                    //Alloc new 2-level table
                    m_LastLvl2Entry = 0;
                    m_LastLvl1Entry++;
                    if (m_LastLvl1Entry > 1023) {
                        //OUT OF MEMORY COMPLETE
                        return false;
                    }
                    uint32_t new_level2_page = 0;
                    try {
                        new_level2_page = g_Manager->getFreePage(false);
                    } catch (exception& e) {
                        return false;
                    }
                    *(level1_entry_mem + 1) = reqMask;
                    *(level1_entry_mem + 1) |= new_level2_page << OFFSET_BITS;
                }
                
                allocated++;
            }
        } else {
            //Free extra pages of memory
            extra = m_MemLimit - pages;
            uint32_t freed = 0;
            bool readOnly = false;
            while (freed < extra) {
                uint32_t level1_offset = m_PageTableRoot & CCPU::ADDR_MASK;
                uint32_t* level1_entry_mem = (uint32_t *)(m_MemStart + level1_offset) + m_LastLvl1Entry;

                uint32_t level2_offset = *level1_entry_mem & CCPU::ADDR_MASK;
                uint32_t* level2_entry_mem = (uint32_t *)(m_MemStart + level2_offset) + m_LastLvl2Entry;
                
                uint32_t current_page = 0;
                if (m_LastLvl2Entry == 0) {
                    m_LastLvl2Entry = 1023;
                    if (m_LastLvl1Entry == 0) {
                        //ATTEMPT TO FREE AFTER COMPLETE DEALLOCATION
                        return false;
                    }
                    m_LastLvl1Entry--;
                    uint32_t current_root_page = *level1_entry_mem >> CCPU::OFFSET_BITS;
                    *level1_entry_mem = 0;
                    g_Manager->deallocatePage(current_root_page);
                    level2_offset = *(level1_entry_mem - 1) & CCPU::ADDR_MASK;
                    level2_entry_mem = (uint32_t *)(m_MemStart + level2_offset) + m_LastLvl2Entry;
                    current_page = *level2_entry_mem >> CCPU::OFFSET_BITS;
                    if ((*level2_entry_mem & reqMask) != reqMask) {
                        readOnly = true;
                    }
                    *level2_entry_mem = 0;
                } else {
                    current_page = *(level2_entry_mem - 1) >> CCPU::OFFSET_BITS;
                    if ((*(level2_entry_mem - 1) & reqMask) != reqMask) {
                        readOnly = true;
                    }
                    *(level2_entry_mem - 1) = 0;
                    m_LastLvl2Entry--;
                }
                if (!readOnly) {
                    g_Manager->deallocatePage(current_page);
                }
                freed++;
            }
        }
        
        m_MemLimit = pages;
        return status;
    }
    
    virtual bool NewProcess(void* processArg, void (*entryPoint)(CCPU*, void*), bool copyMem) {
        bool toManyProcesses = false;
        pthread_mutex_lock(&g_Manager->proc_mtx);
        if (g_Manager->proc_count >= PROCESS_MAX) {
            toManyProcesses = true;
        }
        pthread_mutex_unlock(&g_Manager->proc_mtx);
        if (toManyProcesses) {
            return false;
        }
        ThreadArg *arg = new ThreadArg();
        arg->memStart = m_MemStart;
        arg->processArg = processArg;
        arg->entryPoint = entryPoint;
        arg->copyMem = copyMem;
        arg->parent_cpu = this;
        pthread_t thr_id;
        if (pthread_create(&thr_id, &CIntel::thr_attr, (void*(*)(void*)) thread_func, arg)) {
            return false;
        }
        return true;
    }
    
    bool copyAddressSpace(CIntel *cpu) {
        m_PageTableRoot = g_Manager->allocateFirstLevelTable();
        //SetMemLimit(cpu->GetMemLimit());
        m_MemLimit = cpu->GetMemLimit();
        m_ParentPageTableRoot = cpu->getPageTableRoot();
        
        uint32_t parentPageTableRoot = cpu->getPageTableRoot();
        uint32_t parentLastLvl1Entry = cpu->getLastLvl1Entry();
        
        uint32_t level1_offset = m_PageTableRoot & CCPU::ADDR_MASK;
        uint32_t parent_level1_offset = parentPageTableRoot & CCPU::ADDR_MASK;
        
        pthread_mutex_lock(&g_Manager->proc_mtx);
        for (uint32_t lvl1_entry = 0; lvl1_entry <= parentLastLvl1Entry; lvl1_entry++) {
            uint32_t* level1_entry_mem = (uint32_t *)(m_MemStart + level1_offset) + lvl1_entry;
            uint32_t* parent_level1_entry_mem = (uint32_t *)(m_MemStart + parent_level1_offset) + lvl1_entry;
            
            if ((*level1_entry_mem & reqMask) != reqMask) {
            }
            if ((*parent_level1_entry_mem & reqMask) != reqMask) {
            }
            
            uint32_t level2_offset = *level1_entry_mem & CCPU::ADDR_MASK;
            uint32_t parent_level2_offset = *parent_level1_entry_mem & CCPU::ADDR_MASK;
            
            for (uint32_t lvl2_entry = 0; lvl2_entry < CCPU::PAGE_DIR_ENTRIES; lvl2_entry++) {
                
                uint32_t* level2_entry_mem = (uint32_t *)(m_MemStart + level2_offset) + lvl2_entry;
                uint32_t* parent_level2_entry_mem = (uint32_t *)(m_MemStart + parent_level2_offset) + lvl2_entry;
                
                if (*parent_level2_entry_mem == 0) {
                    break;
                }
                
                if ((*level2_entry_mem & reqMask) != reqMask) {
                }
                
                if ((*parent_level2_entry_mem & reqMask) != reqMask) {
                }
                
                //uint32_t data_offset = *level2_entry_mem & CCPU::ADDR_MASK;
                uint32_t parent_data_offset = (*parent_level2_entry_mem & CCPU::ADDR_MASK) >> CCPU::OFFSET_BITS;
                
                *level2_entry_mem = readOnly;
                *level2_entry_mem |= parent_data_offset << CCPU::OFFSET_BITS;
                m_LastLvl2Entry++;
                
                if (m_LastLvl2Entry > 1023) {
                    m_LastLvl2Entry = 0;
                    m_LastLvl1Entry++;
                    if (m_LastLvl1Entry > 1023) {
                        //OUT OF MEMORY COMPLETE
                        return false;
                    }
                    uint32_t new_level2_page = 0;
                    try {
                        new_level2_page = g_Manager->getFreePage(false);
                    } catch (exception& e) {
                        return false;
                    }
                        *(level1_entry_mem + 1) = reqMask;
                        *(level1_entry_mem + 1) |= new_level2_page << CCPU::OFFSET_BITS;
                }
            }
        }
        pthread_mutex_unlock(&g_Manager->proc_mtx);
        
        return true;
    }
    
protected:

    virtual bool pageFaultHandler(uint32_t address, bool write) {
        if (!write) {
            return false;
        }
        if (m_ParentPageTableRoot == 0) {
            return false;
        }
        
        pthread_mutex_lock(&g_Manager->proc_mtx);
        //TODO copy data page from parent process to child
        const uint32_t reqMask = BIT_PRESENT | BIT_USER | (write ? BIT_WRITE : 0 );
        //const uint32_t orMask = BIT_REFERENCED | (write ? BIT_DIRTY : 0);
        
        uint32_t* level1 = (uint32_t *)(m_MemStart + (m_PageTableRoot & ADDR_MASK)) + (address >> 22);
        if ((*level1 & reqMask) != reqMask) {
            return false;
        }
        uint32_t* parent_level1 = (uint32_t *)(m_MemStart + (m_ParentPageTableRoot & ADDR_MASK)) + (address >> 22);
        if ((*parent_level1 & reqMask) != reqMask) {
            return false;
        }
        
        uint32_t* parent_level2 = (uint32_t *)(m_MemStart + (*parent_level1 & ADDR_MASK )) + ((address >> OFFSET_BITS) & (PAGE_DIR_ENTRIES - 1));
        if ( (*parent_level2 & reqMask ) != reqMask ) {
            return false;
        }
        uint32_t* level2 = (uint32_t *)(m_MemStart + (*level1 & ADDR_MASK )) + ((address >> OFFSET_BITS) & (PAGE_DIR_ENTRIES - 1));
        uint32_t data_page = 0;
        try {
            data_page = g_Manager->getFreePage(true);
        } catch (exception& e) {
            return false;
        }
        *level2 = reqMask;
        *level2 |= data_page << CCPU::OFFSET_BITS;
        if ( (*level2 & reqMask ) != reqMask ) {
            return false;
        }
        
        //*level1 |= orMask;
        //*level2 |= orMask;
        
        uint32_t data_offset = *level2 & CCPU::ADDR_MASK;
        uint32_t parent_data_offset = *parent_level2 & CCPU::ADDR_MASK;
        
        for (uint32_t data_entry = 0; data_entry < CCPU::PAGE_DIR_ENTRIES; data_entry++) {
            uint32_t* data_entry_mem = (uint32_t*)(m_MemStart + data_offset) + data_entry;
            uint32_t* parent_data_entry_mem = (uint32_t*)(m_MemStart + parent_data_offset) + data_entry;
            
            *data_entry_mem = *parent_data_entry_mem;
        }
        //return (uint32_t *)(m_MemStart + (*level2 & ADDR_MASK) + (address & ~ADDR_MASK));
        pthread_mutex_unlock(&g_Manager->proc_mtx);
        return true;
    }
    
};

pthread_attr_t CIntel::thr_attr;

void thread_func(void *arg) {
    ThreadArg *thread_arg = (ThreadArg*) arg;
    void *mem = thread_arg->memStart;
    void *processArg = thread_arg->processArg;
    void (*mainProcess)(CCPU*, void*) = thread_arg->entryPoint;
    bool copyMem = thread_arg->copyMem;
    pthread_mutex_lock(&g_Manager->proc_mtx);
    g_Manager->proc_count++;
    pthread_mutex_unlock(&g_Manager->proc_mtx);
    uint32_t rootTable = 0;
    CIntel *cpu = NULL;
    if (copyMem) {
        cpu = new CIntel((uint8_t*) mem, 0);
        if (!cpu->copyAddressSpace(thread_arg->parent_cpu)) return;
    } else {
        try {
            rootTable = g_Manager->allocateFirstLevelTable();
        } catch (exception& e) {
            return;
        }
        cpu = new CIntel((uint8_t*) mem, rootTable);
    }
    mainProcess(cpu, processArg);
    SAFE_DELETE(cpu);
    SAFE_DELETE(thread_arg);
    pthread_mutex_lock(&g_Manager->proc_mtx);
    g_Manager->proc_count--;
    if (g_Manager->proc_count == 0) {
        pthread_cond_signal(&g_Manager->proc_cond);
    }
    pthread_mutex_unlock(&g_Manager->proc_mtx);
}

void printMemoryDump(void *mem, uint32_t numPages) {
    uint32_t *memory = (uint32_t *) mem;
    for (uint32_t iPage = 0; iPage < numPages; iPage++) {
        uint32_t *m = memory + iPage * CCPU::PAGE_DIR_ENTRIES;
        for (uint32_t entry = 0; entry < CCPU::PAGE_DIR_ENTRIES; entry++) {
            if (m[entry] != 0) {
                cout << m[entry] << " page=" << iPage << " entry=" << entry << endl;
            }
        }
    }
}

void MemMgr(void* mem, uint32_t totalPages, void* processArg, void (*mainProcess)(CCPU*, void*)) {
    g_Manager = new MemoryManager(mem, totalPages);
    pthread_attr_init(&CIntel::thr_attr);
    pthread_attr_setdetachstate(&CIntel::thr_attr, PTHREAD_CREATE_DETACHED);
    
    ThreadArg *arg = new ThreadArg();
    arg->memStart = mem;
    arg->processArg = processArg;
    arg->entryPoint = mainProcess;
    thread_func(arg);
    
    pthread_mutex_lock(&g_Manager->proc_mtx);
    while (g_Manager->proc_count > 0) {
        pthread_cond_wait(&g_Manager->proc_cond, &g_Manager->proc_mtx);
    }
    pthread_mutex_unlock(&g_Manager->proc_mtx);
    
    SAFE_DELETE(g_Manager);
}
