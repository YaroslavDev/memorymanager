### Memory Manager

Simulation of Memory Management in UNIX-like operating system. Simulated manager handles allocation and freeing of "physical" memory, page fault handling and multiprocessing.

Check out MemoryManager/MemoryManager.pdf for task description.